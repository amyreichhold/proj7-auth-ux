"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import json
from flask import Flask, request, render_template, redirect, url_for, flash, make_response
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, BooleanField
from wtforms.validators import DataRequired
from passlib.apps import custom_app_context as pwd_context

import logging
from flask_cors import CORS

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time

def generate_auth_token(expiration=600):
   s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   #s = Serializer('test1234@#$', expires_in=expiration)
   # pass index of user
   return s.dumps({'id': 1})

def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"

def hash_password(password):
        return pwd_context.encrypt(password)

def verify_password(password, hashVal):
        return pwd_context.verify(password, hashVal)

# your user class
class User(UserMixin):
    def __init__(self, id, name, password, active=True):
        self.id = id
        self.name = name
        self.password = password
        self.active = active

    def is_active(self):
        return self.active

class MyForm(FlaskForm):
        username = StringField('Enter username', validators=[DataRequired()])
        password = PasswordField('Enter password', validators=[DataRequired()])
        checkbox = BooleanField('Remember me?')

'''
USER_NAMES = {}
user_count = 0

def create_user(username, password):
    hashed_password = hash_password(password)

    # the database assigns its own unique ID so the user_count won't be needed
    # later
    global user_count
    USER_NAMES[username] = User(user_count, username, hashed_password)
    user_count += 1
    return

create_user(u"Billie", "123")
create_user(u"A", "123")
create_user(u"B", "123")
'''

###
app = flask.Flask(__name__)
cors = CORS(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient('db', 27017)
#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#db = client.tododb
db = client.badguy.db

###
# Pages
###


#step 1 in slides
login_manager = LoginManager()

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

login_manager.setup_app(app)

def get_user_names():
    _users = db.users.find()
    users = [user for user in _users]
    users_dict = {}
    for user in users:
        user_id = user["_id"]
        username = user["username"]
        password = user["password"]
        users_dict[username] = User(user_id, username, password)
    return users_dict

@login_manager.user_loader
def load_user(id):
    #user_id = int(id)
    user_id = str(id)
    USER_NAMES = get_user_names()
    for name in USER_NAMES:
        user_object = USER_NAMES[name]
        if user_object.id == user_id:
            return user_object
    return None

@login_manager.request_loader
def load_user_from_request(request):
    api_key = request.args.get('api_key')
    if api_key:
        user = User.query.filter_by(api_key=api_key).first()
        if user:
            return user
    
    api_key = request.headers.get('Authorization')
    if api_key:
        api_key = api_key.replace('Basic ', '', 1)
        try:
            api_key = base64.b64decode(api_key)
        except TypeError:
            pass
        user = User.query.filter_by(api_key=api_key).first()
        if user:
            return user

    return None


''' we could try this? if request_loader doesn't work
# https://flask-login.readthedocs.io/_/downloads/en/latest/pdf/
@login_manager.header_loader
def load_user_from_header(header_val):
    header_val = header_val.replace('Basic ', '', 1)
    try:
        header_val = base64.b64decode(header_val)
    except TypeError:
        pass
    return User.query.filter_by(api_key=header_val).first()
'''

@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")

#step 3 in slides
# This is one way. Using WTForms is another way.
@app.route("/login", methods=["GET", "POST"])
def login():
    form = MyForm()
    if form.validate_on_submit():
    #if request.method == "POST" and "username" in request.form:
        username = request.form["username"]
        password = request.form["password"]
        USER_NAMES = get_user_names()
        if username in USER_NAMES:
            if not verify_password(password, USER_NAMES[username].password):
                flash("Incorrect password! Please try again")
                return redirect(url_for("login"))
            remember = request.form.get("remember", "no") == "yes"
            if login_user(USER_NAMES[username], remember=remember):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("index"))
            else:
                flash("Sorry, but you could not log in.")
        else:
            flash(u"Invalid username, please sign up.")
            return redirect(url_for("sign_up"))
    return render_template("login.html", form=form)

@app.route("/sign_up", methods=["GET", "POST"])
def sign_up():
    USER_NAMES = get_user_names()
    form = MyForm()
    if form.validate_on_submit():
    #if request.method == "POST" and "username" in request.form:
        username = request.form["username"]
        password = request.form["password"]
        if username not in USER_NAMES:
            create_user(username, password)
            #global user_count
            #USER_NAMES[username] = User(username, user_count, hash_password(password))
            #user_count += 1
            remember = request.form.get("remember", "no") == "yes"
            if login_user(USER_NAMES[username], remember=remember):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("index"))
            else:
                flash("Sorry, but you could not log in.")
        else:
            flash(u"Invalid username, username already exists.")
            return redirect(url_for("sign_up"))
    return render_template("sign_up.html", form=form)


# step 5 in slides
@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("login"))

@app.route("/register", methods=["GET"])
def register():
    form = MyForm()
    return render_template("register.html", form=form)

@app.route("/api/users/<username>", methods=["GET"])
def api_users_username(username):
    brussels_sprouts = {}
    status_code = 500
    USER_NAMES = get_user_names()
    if username in USER_NAMES:
        brussels_sprouts = {
            u"username": username,
            u"password": USER_NAMES[username].password,
            u"user_id": USER_NAMES[username].id
                }
        status_code = 201
    else:
        status_code = 400
    return flask.jsonify(brussels_sprouts), status_code

@app.route("/api/register", methods=["POST"])
def api_register():
    username = request.form["username"]
    status_code = 500
    brussels_sprouts = {}
    user_names = get_user_names()
    user_id = -1
    if username not in user_names:
        #create_user(username, password)
        hashed_password = hash_password(request.form["password"])
        tater_tot = {
                u"username": username,
                u"password": hashed_password,
            }
        db.users.insert_one(tater_tot)

        # get id
        brussels_sprouts = {
                u"username": username,
                u"password": hashed_password,
                u"user_id" : str(tater_tot['_id'])
            }
        status_code = 201
    else:
        status_code = 400
    response = make_response(flask.jsonify(brussels_sprouts), status_code)
    response.headers['Location'] = u'/api/users/{}'.format(username)
    return response

@app.route("/api/token", methods=["GET"])
def api_token():
    # Ram's basic auth example doesn't satisfy this?
    #if load_user_from_request(request):
    #    return flask.jsonify(request)
    username = request.args["username"]
    password = request.args["password"]
    USER_NAMES = get_user_names()
    if username in USER_NAMES:
        if not verify_password(password, USER_NAMES[username].password):
            return flask.jsonify({}), 401
        remember = request.args.get("remember", "no") == "yes"
        if login_user(USER_NAMES[username], remember=remember):
            # change this to use config later possibly
            d = 10
            t = generate_auth_token(d)
            brussels_sprouts = {
                    u'token': str(t),
                    u'duration': str(d)
                }
            return flask.jsonify(brussels_sprouts)
        else:
            return flask.jsonify({}), 401
    else:
        return flask.jsonify({}), 401

@app.route("/listall/<representation>")
@app.route("/listAll/<representation>")
def listAll(representation):
    if 'token' in request.args:
        token = request.args.get('token')
        if verify_auth_token(token):
            _items = db.brevet.find()
            items = [item for item in _items]
            _controles = db.controle.find()
            controles = []
            for controle in _controles:
                hamstring = {}
                for key, value in controle.items():
                    if key != '_id':
                        hamstring[key] = value
                if hamstring['km'] != "":
                    controles.append(hamstring)
            message = ''
            if len(controles) == 0:
                message = 'Nothing submitted yet'
            truncate = len(controles)
            top = request.args.get('top')
            if top is not None:
                truncate = int(top)
                if truncate > len(controles):
                    truncate = len(controles)
                elif truncate < 0:
                    truncate = 0
                else:
                    pass
            controles = controles[:truncate]

            if representation.lower() == 'json':
                potato = {
                         'controles': controles
                    }
                return flask.jsonify(potato)
            elif representation.lower() == 'csv':
                headers = []
                big_boi = ''
                first_controle = controles[0]
                for key in first_controle:
                    headers.append(key)
                big_boi += ",".join(headers) + '\n'
                for controle in controles:
                    row = []
                    for header in headers:
                        row.append(controle[header])
                    big_boi += ",".join(row) + '\n'
                response = make_response(big_boi, 200)
                response.mimetype = "text/plain"
                return response
            else:
                return flask.jsonify({}), 401
        else: # token not verified
            return flask.jsonify({}), 401
    else: # no token in request
        return flask.jsonify({}), 401


@app.route("/listall")
@app.route("/listAll")
def listAllJson():
    return listAll('json')

@app.route("/listopenonly/<representation>")
@app.route("/listOpenOnly/<representation>")
def listOpenOnly(representation):
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = []
    for controle in _controles:
        hamstring = {}
        for key, value in controle.items():
            if key != '_id' and key != 'close':
                hamstring[key] = value
        if hamstring['km'] != "":
            controles.append(hamstring)
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    truncate = len(controles)
    top = request.args.get('top')
    if top is not None:
        truncate = int(top)
        if truncate > len(controles):
            truncate = len(controles)
        elif truncate < 0:
            truncate = 0
        else:
            pass
    controles = controles[:truncate]

    if representation.lower() == 'json':
        potato = {
                 'controles': controles
            }
        return flask.jsonify(potato)
    elif representation.lower() == 'csv':
        headers = []
        big_boi = ''
        first_controle = controles[0]
        for key in first_controle:
            headers.append(key)
        big_boi += ",".join(headers) + '\n'
        for controle in controles:
            row = []
            for header in headers:
                row.append(controle[header])
            big_boi += ",".join(row) + '\n'
        response = make_response(big_boi, 200)
        response.mimetype = "text/plain"
        return response
    else:
        return 'not today satan'

@app.route("/listOpenOnly")
@app.route("/listopenonly")
def listOpenOnlyJson():
    return listOpenOnly('json')

@app.route("/listcloseonly/<representation>")
@app.route("/listCloseOnly/<representation>")
def listCloseOnly(representation):
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = []
    for controle in _controles:
        hamstring = {}
        for key, value in controle.items():
            if key != '_id' and key != 'open':
                hamstring[key] = value
        if hamstring['km'] != "":
            controles.append(hamstring)
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    truncate = len(controles)
    top = request.args.get('top')
    if top is not None:
        truncate = int(top)
        if truncate > len(controles):
            truncate = len(controles)
        elif truncate < 0:
            truncate = 0
        else:
            pass
    controles = controles[:truncate]

    if representation.lower() == 'json':
        potato = {
                 'controles': controles
            }
        return flask.jsonify(potato)
    elif representation.lower() == 'csv':
        headers = []
        big_boi = ''
        first_controle = controles[0]
        for key in first_controle:
            headers.append(key)
        big_boi += ",".join(headers) + '\n'
        for controle in controles:
            row = []
            for header in headers:
                row.append(controle[header])
            big_boi += ",".join(row) + '\n'
        response = make_response(big_boi, 200)
        response.mimetype = "text/plain"
        return response
    else:
        return 'not today satan'

@app.route("/listCloseOnly")
@app.route("/listcloseonly")
def listCloseOnlyJson():
    return listCloseOnly('json')


@app.route("/")
@app.route("/index", methods=['GET', 'POST'])
def index():
    app.logger.debug("Main page entry")
    if request.method == 'POST':
        db.brevet.drop()
        db.controle.drop()
        item_doc = {
            'distance': request.form['distance'],
            'begin_date': request.form['begin_date'],
            'begin_time': request.form['begin_time']
        }
        db.brevet.insert_one(item_doc)
        item_docs = []
        for i,miles in enumerate(request.form.getlist('miles')):
            item_docs.append({
                    'miles': miles
                    })
        for i,km in enumerate(request.form.getlist('km')):
            item_docs[i]['km'] = km
        for i,location in enumerate(request.form.getlist('location')):
            item_docs[i]['location'] = location
        for i,open in enumerate(request.form.getlist('open')):
            item_docs[i]['open'] = open
        for i,close in enumerate(request.form.getlist('close')):
            item_docs[i]['close'] = close

        for item_doc in item_docs:
            db.controle.insert_one(item_doc)

    return flask.render_template('calc.html')

@app.route('/new', methods=['POST'])
def potato():
    item_doc = {
        'distance': request.form['distance'],
        'begin_date': request.form['begin_date']
    }
    
    db.tododb.insert_one(item_doc)
    
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html')
    #return str('{distance} {begin_date}').format(distance=item_doc["distance"], begin_date=item_doc["begin_date"])


@app.route('/display')
def display():
    _items = db.brevet.find()
    items = [item for item in _items]
    _controles = db.controle.find()
    controles = [controle for controle in _controles]
    message = ''
    if len(controles) == 0:
        message = 'Nothing submitted yet'
    return render_template('display.html', items=items, controles=controles, message=message)
    #return redirect(url_for('index')) 


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_start_time = request.args.get('brevet_start_time', "2017-01-01T00:00:00-07:00", type=str)
    brevet_dist_km = request.args.get('brevet_dist_km', 200, type=int)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, brevet_start_time)
    close_time = acp_times.close_time(km, brevet_dist_km, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
