"""
Front end/user interface
"""

import os
import flask
import arrow  # Replacement for datetime, based on moment.js
import config
#from config import configuration
import json
from flask import Flask, request, render_template, redirect, url_for, flash, make_response
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, BooleanField
from wtforms.validators import DataRequired
from passlib.apps import custom_app_context as pwd_context

import logging
from flask_cors import CORS

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time


# your user class
class User(UserMixin):
    def __init__(self, id, name, password, active=True):
        self.id = id
        self.name = name
        self.password = password
        self.active = active

    def is_active(self):
        return self.active

class MyForm(FlaskForm):
        username = StringField('Enter username', validators=[DataRequired()])
        password = PasswordField('Enter password', validators=[DataRequired()])
        checkbox = BooleanField('Remember me?')

###
app = flask.Flask(__name__)
cors = CORS(app)
CONFIG = config.configuration()
#CONFIG = configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


#step 1 in slides
login_manager = LoginManager()

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

login_manager.setup_app(app)

def get_user_names():
    _users = db.users.find()
    users = [user for user in _users]
    users_dict = {}
    for user in users:
        user_id = user["_id"]
        username = user["username"]
        password = user["password"]
        users_dict[username] = User(user_id, username, password)
    return users_dict

@login_manager.user_loader
def load_user(id):
    '''
    #user_id = int(id)
    user_id = str(id)
    USER_NAMES = get_user_names()
    for name in USER_NAMES:
        user_object = USER_NAMES[name]
        if user_object.id == user_id:
            return user_object
    '''
    return None

@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")

#step 3 in slides
# This is one way. Using WTForms is another way.
@app.route("/login", methods=["GET"])
def login():
    form = MyForm()
    '''
    if form.validate_on_submit():
    #if request.method == "POST" and "username" in request.form:
        username = request.form["username"]
        password = request.form["password"]
        USER_NAMES = get_user_names()
        if username in USER_NAMES:
            if not verify_password(password, USER_NAMES[username].password):
                flash("Incorrect password! Please try again")
                return redirect(url_for("login"))
            remember = request.form.get("remember", "no") == "yes"
            if login_user(USER_NAMES[username], remember=remember):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("index"))
            else:
                flash("Sorry, but you could not log in.")
        else:
            flash(u"Invalid username, please sign up.")
            return redirect(url_for("sign_up"))
    '''
    return render_template("login.html", form=form)

# step 5 in slides
@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("login"))

@app.route("/register", methods=["GET"])
def register():
    form = MyForm()
    return render_template("register.html", form=form)

@app.route("/")
@app.route("/index", methods=['GET'])
def index():
    logged_in = False
    if logged_in:
        pass
    else:
        return redirect(url_for("login"))

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
